/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.EtchedBorder;

/**
 *
 * @author Don Comedia
 */
public class MenuPrincipal extends JFrame{
    
    public MenuPrincipal() {
		inicializar();
		dimensionar();
		adicionar();
		visualizar();
                accionar();

    }

    private JLabel etq1;
    private JButton jb1;
    private JButton jb2;
    private JButton jb3;
    private JButton jb4;
    private JLabel parteAbajo;
    private JLabel parteArriba;
    
    public void inicializar() {
        this.setLayout(null);
      
        this.etq1= new JLabel("Menu Principal");
        this.jb1 = new JButton("Materias");
        this.jb2 = new JButton("Grupos");
        this.jb3 = new JButton("Estudiantes");
        this.jb4 = new JButton("Profesores");
        
        
        this.parteAbajo = new JLabel();
        this.parteAbajo.setIcon(new ImageIcon("src/img/parteAbajoTodo.png"));
         this.parteArriba = new JLabel();
        this.parteArriba.setIcon(new ImageIcon("src/img/parteArribaTodo.png"));
    }
    
    public void dimensionar() {

        this.etq1.setBounds(700, 230, 175, 100);
        this.jb1.setBounds(450, 330 , 100, 80);
        this.jb2.setBounds(600, 330, 100, 80);
        this.jb3.setBounds(750, 330, 130, 80);
        this.jb4.setBounds(930, 330, 100, 80);
        
        this.parteAbajo.setBounds(0,768,1344,59);
        this.parteArriba.setBounds(0,0,1344,59);
    }
    
    public void adicionar() {
  
        this.add(this.etq1);
        this.add(this.jb1);
        this.add(this.jb2);
        this.add(this.jb3);
        this.add(this.jb4);
        this.add(this.parteAbajo);
        this.add(this.parteArriba);
    }
    
    public void visualizar() {
        this.setSize(1344, 865);
        this.setVisible(true);
        
        this.getContentPane().setBackground(Color.WHITE);
    
    }
    
       public void cerrar(){
            this.setVisible(false);
        }
    
    
          	public void accionar() {
		this.jb1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				System.out.println("hola mesnaje de prueba ");
                                RegistrarMateria regMat = new RegistrarMateria();
                                cerrar();
                      
			}
		});
	}
    
}
